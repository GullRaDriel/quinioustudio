#ifndef HEADER_7502427723DA80CA
#define HEADER_7502427723DA80CA

#ifndef QuiniouStudioConfigHeader
#define QuiniouStudioConfigHeader

#ifdef __cplusplus
extern "C"
{
#endif

#include <allegro5/allegro.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>

#include "nilorea/n_list.h"
#include "nilorea/n_log.h"
#include "nilorea/n_str.h"

#define NB_MAX_TITLES 256
#define SMALL_FONT 0
#define REGULAR_FONT 1
#define HUGE_FONT 2

#ifdef FLOAT_AUDIO
   typedef float audio_t ;
#else
   typedef int16_t audio_t ;
   static const audio_t min_sample_val=0x8000;
   static const audio_t max_sample_val=0x7fff;
   static const int sample_range  =0xffff;
#endif


   /*! Quiniou Studio Config structure */
   typedef struct QS_INTERNALS
   {
      /* DISPLAY */
      /*! path to ttf font file */
      const char *font_file_small ,
            /*! path to ttf font file */
            *font_file_regular ,
            /*! path to ttf font file */
            *font_file_huge ,
            /*! path to background picture */
            *background_file ;
      /*! background image */
      ALLEGRO_BITMAP *bg ;

      /*! size for little text output */
      int font_size_small ,
          /*! size for normal text output */
          font_size_regular ,
          /*! size for huge text output */
          font_size_huge ,
          /*! Screen W size */
          w ,
          /*! Screen H size */
          h ;
      /*! Internal loop freq */
      float refresh_rate ;

      /*! The 3 loaded fonts with their respective sizes */
      ALLEGRO_FONT *font[ 3 ];
      /*! Acutal display */
      ALLEGRO_DISPLAY *display ;
      /*! Event queue */
      ALLEGRO_EVENT_QUEUE *queue ;
      /*! Timer */
      ALLEGRO_TIMER *timer ;
      /*! FPS Timer */
      ALLEGRO_TIMER *fps_timer ;
      /*! visualisation nb_sampels */
      float nb_viz_seconds ;
      /*! internal: current number of displayed samples */
      int nb_displayed_samples ;
      /*! internal: display mod */
      unsigned int fullscreen ;
      /*! internal: title scroll position */
      int title_scroll_position ;

      /*! RECORDER */
      ALLEGRO_AUDIO_RECORDER *recorder ;
      /*! fragment from recorder */
      ALLEGRO_AUDIO_RECORDER_EVENT *record_event ;
      /*! number of channel. For now fixed to 2 */
      int channels ,
	  /*! minimum sample size in secs */
	      nb_min_secs ;
      /*! recording buffer , stores up to max_seconds of audio */
      audio_t *record_buffer,
              /*! points to the current recorded position */
              *record_buffer_pos,
              /*! points to the end of the buffer */
              *record_buffer_end,
              /*! temporary sample storage */
              *tmp_buffer;
      /*! temporary buf size */
      unsigned int tmp_buffer_size ;

      /*! title edit flag */
      unsigned int is_editing ;
      /*! recording flag */
      unsigned int is_recording ;
      /*! Recording frequency (44100,48000,96000) */
      unsigned int frequency ;
      /*! Maximum size of sample */
      unsigned int max_seconds ;
      /*! recorder internal buffer */
      float fragment ;
      /*! Latency */
      float latency ,
            /*! Computed volume for current input */
            gain ;


      /* TITLE LIST */
       /*! xpos */
      int title_x ;
      /*! ypos */
      int title_y ;
      /*! width */
      int title_w ;
      /*! height */
      int title_h ;
      /*! colors */
      ALLEGRO_COLOR title_colors[ 4 ];

      /* WAVEFORM */
       /*! xpos */
      int wave_x ;
      /*! ypos */
      int wave_y ;
      /*! width */
      int wave_w ;
      /*! height */
      int wave_h ;
      /*! colors */
      ALLEGRO_COLOR wave_colors[ 4 ];

      /* VOLUMEMETER */
       /*! xpos */
      int vol_x ;
      /*! ypos */
      int vol_y ;
      /*! width */
      int vol_w ;
      /*! height */
      int vol_h ;
      /*! colors */
      ALLEGRO_COLOR vol_colors[ 4 ];

      /*! Loaded title list */
      ALLEGRO_USTR *titles[ NB_MAX_TITLES + 1 ];
      /*! number of loaded titles */
      int nb_titles ;


   } QS_INTERNALS ;

   /* abort and tell the problem in a native dialog box if possible */
   void abort_qvstudio(char const *format, ...);
   /* get a color from a r,g,b,a string  */
   ALLEGRO_COLOR get_color_from_string( const char *str , char *cfg_param );
   /* load global QuiniouStudio config */
   QS_INTERNALS *load_qs_config( char *file );
   /* initialize after laoding config */
   int init_qs( QS_INTERNALS *qvstudio );

#ifdef __cplusplus
}
#endif

#endif
#endif // header guard

