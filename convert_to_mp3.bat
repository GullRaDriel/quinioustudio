@echo off
setlocal enabledelayedexpansion
REM Recreate directory tree
xcopy /t /e .\output\* .\output_mp3\
REM Scan for songs
for /f "delims=" %%f in ('dir /s /b .\output\*.wav') do @(
set file=%%f
set new_dir=!file:\output\=\output_mp3\!
set new_file=!new_dir:.wav=.mp3!
REM If no mp3 version, then make it 
IF NOT EXIST "!new_file!" ffmpeg.exe -i "!file!" -codec:a libmp3lame -qscale:a 2 "!new_file!"
)