#include "QSConfig.h"
#include "QSGfx.h"
#include "GL/gl.h"
#include <sys/timeb.h>

struct timeb tmb ;

QS_INTERNALS *qvstudio = NULL ;
ALLEGRO_USTR *title = NULL ;

int w = 0 , h = 0 , fps = 0 , fps_count = 0 , disp_carret = 0 ;

int main( int argc , char *argv[] )
{
	/* Remove warn for unused argc,argv */
	(void) argc; (void) argv;

	qvstudio = load_qs_config( "QuiniouStudio.cfg" );
	if( !qvstudio )
	{
		abort_qvstudio( "Unable to load config or init problem" );
	}
	N_STR *nstr = NULL ;
	nstrprintf( nstr , "QuiniouStudio [Build on %s at %s]" , __DATE__  , __TIME__ );
	al_set_window_title( qvstudio -> display, _nstr( nstr ) );

	int selected_title = 0 ;
	int mx = 0 ,  my =  0 ;
	audio_t low=0 , high = 0 ;
	float cur_time = 0 ;

	ALLEGRO_BITMAP *scrbuf = al_create_bitmap( qvstudio -> w , qvstudio -> h );
	al_set_target_bitmap( scrbuf );
	al_clear_to_color( al_map_rgba( 0 , 0 , 0 , 255 ) );

	int font_height = al_get_font_line_height( qvstudio -> font [ REGULAR_FONT ] );
	qvstudio -> nb_displayed_samples = qvstudio -> frequency / 2 ;
	al_flush_event_queue( qvstudio -> queue );

	fps = qvstudio -> refresh_rate ;

	title = al_ustr_new( "Change Title" );

	const unsigned char *renderer = glGetString(GL_RENDERER);
	while (true)
	{
		ALLEGRO_EVENT event;
		bool do_draw = false;
		/*		bool got_event = false ;
				while( al_get_next_event( qvstudio -> queue, &event) != false )
				{
				got_event = true ;*/
		al_wait_for_event( qvstudio -> queue, &event);

		if( event.type == ALLEGRO_EVENT_AUDIO_RECORDER_FRAGMENT && qvstudio -> recorder != NULL )
		{

			unsigned int i;
			low=high=0;

			qvstudio -> record_event = al_get_audio_recorder_event(&event);

			int samples_to_copy = qvstudio -> channels * qvstudio -> record_event ->samples;

			for( i = qvstudio -> tmp_buffer_size - samples_to_copy ; i < qvstudio -> tmp_buffer_size ; ++i )
			{
				if (qvstudio -> tmp_buffer[i] < low)
				{
					low =  qvstudio -> tmp_buffer[ i ];
				}
				else if (qvstudio -> tmp_buffer[i] > high)
				{
					high = qvstudio -> tmp_buffer[i];
				}
			}
#ifdef FLOAT_AUDIO
			float gain = (high - low) / 2.0 ;
#else
			float gain =  MAX( abs( high ) , abs( low ) ) / (float) max_sample_val ;
#endif

			if( gain < 0.0 )
				gain = 0.0 ;
			if(  gain > 1.0 )
				gain = 1.0 ;

			qvstudio -> gain = 0.25 * qvstudio -> gain + 0.75 * gain ;
			if( samples_to_copy )
			{
				if( (unsigned)samples_to_copy  > qvstudio -> tmp_buffer_size )
				{
					qvstudio -> tmp_buffer_size = samples_to_copy ;
					qvstudio -> tmp_buffer = realloc(  qvstudio -> tmp_buffer , samples_to_copy * sizeof( audio_t ) );
					fprintf( stderr , "Reallocing !" );
				}
				memmove(  qvstudio -> tmp_buffer ,  qvstudio -> tmp_buffer + samples_to_copy , (qvstudio -> tmp_buffer_size - samples_to_copy )* sizeof( audio_t ) );
				memcpy(  qvstudio -> tmp_buffer + qvstudio -> tmp_buffer_size - samples_to_copy ,  qvstudio -> record_event -> buffer , samples_to_copy* sizeof( audio_t ) );
			}
			if( qvstudio -> is_recording == 1 )
			{
				if( (unsigned)( samples_to_copy * sizeof( audio_t ) ) >= (unsigned)( qvstudio -> record_buffer_end - qvstudio -> record_buffer_pos ) )
					goto forced_sample_save ;

				if (samples_to_copy)
				{
					memcpy( qvstudio -> record_buffer_pos , qvstudio -> record_event->buffer , samples_to_copy * sizeof( audio_t ) );
				}
				qvstudio -> record_buffer_pos += samples_to_copy ;
			}
			continue ;
		}
		else
		{
			if( qvstudio -> is_editing == 1)
						get_keyboard( title , event );

			switch( event.type )
			{
				case ALLEGRO_EVENT_KEY_CHAR :

					if( event.keyboard.keycode == ALLEGRO_KEY_PAD_MINUS )
					{
						qvstudio -> nb_displayed_samples -= 5000 ;
					}
					if( event.keyboard.keycode == ALLEGRO_KEY_PAD_PLUS )
					{
						qvstudio -> nb_displayed_samples += 5000 ;
					}
					if( event.keyboard.keycode == ALLEGRO_KEY_ENTER && qvstudio -> is_editing == 1 )
					{
						qvstudio -> is_editing = 0 ;
						if( qvstudio -> titles[ selected_title ] )
							al_ustr_free(  qvstudio -> titles[ selected_title ] );
						if( al_ustr_length( title ) > 0 )
						{
							qvstudio -> titles[ selected_title ]  = al_ustr_dup( title );
						}
						else
						{
							for( int it = selected_title ; it < qvstudio -> nb_titles ; it ++ )
							{
								qvstudio -> titles[ it ] = qvstudio -> titles[ it + 1 ] ;
							}
							qvstudio -> nb_titles -- ;
							if( selected_title >= qvstudio -> nb_titles )
								selected_title =  qvstudio -> nb_titles -1 ;
							if( selected_title < 0 )
								selected_title = 0 ;
						}
						/* also write modif to file */
						FILE *out = fopen( "title_list.txt"  ,"w" );
						if( !out )
						{
							abort_qvstudio("Unable to open title_list.txt\n");
						}
						int it = 0 ;
						while( 	qvstudio -> titles[ it ] )
						{
							char cbuf[256]="";
							al_ustr_trim_ws( qvstudio -> titles[ it ] ) ;
							al_ustr_to_buffer(  qvstudio -> titles[ it ] , cbuf , 256 );
							fprintf( out , "%s\n" , cbuf );
							it ++ ;
						}
						fclose( out );

					}
					break ;


				case ALLEGRO_EVENT_MOUSE_AXES:
					mx = event.mouse.x;
					my = event.mouse.y;
					qvstudio -> title_scroll_position -= event.mouse.dz ;
					if( qvstudio -> title_scroll_position < 0 ) qvstudio -> title_scroll_position = 0 ;
					if( qvstudio -> title_scroll_position > qvstudio -> nb_titles - 1 )
						qvstudio -> title_scroll_position = qvstudio -> nb_titles - 1 ;
					break;

				case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
				if( qvstudio -> fullscreen == 0 )
					{
						if( mx > qvstudio -> title_x && mx < ( qvstudio -> title_x + qvstudio -> title_w - 20 ) &&
							my > qvstudio -> title_y && my < ( qvstudio -> title_y + qvstudio -> title_h - 20 ) )
						{
							int new_selection = ( ( my - qvstudio -> title_y ) / font_height ) + qvstudio -> title_scroll_position ;
							if( new_selection >= 0 && new_selection < qvstudio -> nb_titles )
							{
								selected_title = new_selection ;

								if( event.mouse.button == 2 && qvstudio -> is_editing == 0 && qvstudio -> nb_titles > 0 )
								{
									al_ustr_free( title );
									title = al_ustr_dup( qvstudio -> titles[ selected_title ] );
									qvstudio -> is_editing = 1 ;
								}
							}
						}
					}
					else
					{
						al_acknowledge_resize( qvstudio -> display );
						w = al_get_display_width(  qvstudio -> display );
						h = al_get_display_height( qvstudio -> display );

						mx = mx - (w / 2) + (qvstudio -> w / 2 );
						my = my - (h / 2) + (qvstudio -> h / 2 );

						if( mx > qvstudio -> title_x && mx < ( qvstudio -> title_x + qvstudio -> title_w - 20 ) &&
								my > qvstudio -> title_y && my < ( qvstudio -> title_y + qvstudio -> title_h ) )
						{
							int new_selection = ( ( my - qvstudio -> title_y ) / font_height ) + qvstudio -> title_scroll_position ;
							if( new_selection >= 0 && new_selection < qvstudio -> nb_titles )
							{
								selected_title = new_selection ;

								if( event.mouse.button == 2 && qvstudio -> is_editing == 0 && qvstudio -> nb_titles > 0 )
								{
									al_ustr_free( title );
									title = al_ustr_dup( qvstudio -> titles[ selected_title ] );
									qvstudio -> is_editing = 1 ;
								}
							}
						}
					}
					if( qvstudio -> nb_titles > 0 )
					{
						ALLEGRO_USTR *str = al_ustr_new( "QuiniouStudio, en cours:" );
						al_ustr_append( str , qvstudio -> titles[selected_title] );
						al_set_window_title(qvstudio->display, al_cstr( str ) );
						free_nstr( &nstr );
					}
					break;


				case ALLEGRO_EVENT_KEY_DOWN:
					if (event.keyboard.keycode == ALLEGRO_KEY_F12 && qvstudio -> is_recording == 0 ) {
						bool opp = !(al_get_display_flags(qvstudio -> display) & ALLEGRO_FULLSCREEN_WINDOW);
						al_set_display_flag(qvstudio -> display, ALLEGRO_FULLSCREEN_WINDOW, opp);
						al_flush_event_queue( qvstudio -> queue );
						qvstudio -> fullscreen = opp ;
					}

					if( event.keyboard.keycode == ALLEGRO_KEY_PAD_ENTER && qvstudio -> is_editing == 0 )
					{
						if( qvstudio -> nb_titles == 0 )
							selected_title = 0 ;
						else
						{
							al_ustr_free( title );
							title = al_ustr_new( "new title" );
							selected_title =  qvstudio -> nb_titles ;
						}
						qvstudio -> is_editing = 1 ;
						qvstudio -> nb_titles ++ ;
					}

					if( event.keyboard.keycode == ALLEGRO_KEY_SPACE && qvstudio -> is_editing == 0 )
					{
						do
						{
							al_wait_for_event( qvstudio -> queue, &event);
						}while( event.type == ALLEGRO_EVENT_KEY_DOWN );
						if( qvstudio -> is_recording == 1 )
						{
							char current_time_string[ 64 ] = "" ;
							struct tm *local_time = NULL ;
							time_t current_time = 0 ;
							qvstudio -> is_recording = 0 ;
forced_sample_save:
							if( cur_time >= qvstudio -> nb_min_secs )
							{
							current_time = time( NULL );   // get time now
							local_time = localtime( &current_time );
							if( strftime( current_time_string , sizeof( current_time_string ) , "%S %M %H %d %m %w" , local_time ) == 0 )
							{
								n_log( LOG_ERR , "strftime returned 0" );
							}
							char **time_split = split( current_time_string , " " , 0 );
							N_STR *date_str = NULL ;
							N_STR *command = NULL ;
#ifndef WIN32
							nstrprintf( date_str , "./output/%d/%s/%s" , local_time -> tm_year + 1900  , time_split[ 4 ] , time_split[ 3 ] );
							nstrprintf( command , "mkdir -p %s" , date_str -> data );
#else
							nstrprintf( date_str , "output\\%d\\%s\\%s" , local_time -> tm_year + 1900  , time_split[ 4 ] , time_split[ 3 ] );
							nstrprintf( command , "mkdir %s" , date_str -> data );
#endif
							system( command -> data );
							nstrcat_bytes( date_str , "/" );
							char *selected_title_cstr = al_cstr_dup( qvstudio -> titles[ selected_title ] );
							nstrcat_bytes( date_str , selected_title_cstr );
							free( selected_title_cstr );
							nstrcat_bytes( date_str , "_" );
							nstrcat_bytes( date_str , time_split[ 3 ] );
							nstrcat_bytes( date_str , "-" );
							nstrcat_bytes( date_str , time_split[ 4 ] );
							nstrcat_bytes( date_str , "-" );
							nstrprintf_cat( date_str ,"%d" , local_time -> tm_year + 1900 );
							nstrcat_bytes( date_str , "_" );
							nstrcat_bytes( date_str , time_split[ 2 ] );
							nstrcat_bytes( date_str , "H" );
							nstrcat_bytes( date_str , time_split[ 1 ] );
							nstrcat_bytes( date_str , "M" );
							nstrcat_bytes( date_str , time_split[ 0 ] );
							nstrcat_bytes( date_str , "S" );
							nstrcat_bytes( date_str , ".wav" );
							/* finished recording, but haven't created the sample yet */
							al_draw_filled_rectangle( 50 , 50 , w - 50 , h - 50 , al_map_rgb( 0 ,0 ,0 ) );
							al_draw_rectangle( 50 , 50 , w - 50 , h - 50 , al_map_rgb( 255 ,0 , 0 ) , 1.0 );
							al_draw_text( qvstudio -> font[ HUGE_FONT ] , al_map_rgb( 255 , 0 , 0 ) ,
									w/2 , h/2 - font_height , ALLEGRO_ALIGN_CENTER , "SAVING" );
							al_draw_text( qvstudio -> font[ REGULAR_FONT ] , al_map_rgb( 255 , 0 , 0 ) ,
									w/2 , h/2 + 2 * font_height , ALLEGRO_ALIGN_CENTER , _nstr( date_str ) );
							al_flip_display();
#ifdef FLOAT_AUDIO
							ALLEGRO_SAMPLE *spl = al_create_sample( qvstudio -> record_buffer ,
									( qvstudio -> record_buffer_pos - qvstudio -> record_buffer ) / 2 ,
									qvstudio -> frequency ,
									ALLEGRO_AUDIO_DEPTH_FLOAT32,
									ALLEGRO_CHANNEL_CONF_2 , false );
#else
							ALLEGRO_SAMPLE *spl = al_create_sample( qvstudio -> record_buffer ,
									( qvstudio -> record_buffer_pos - qvstudio -> record_buffer ) / 2 ,
									qvstudio -> frequency ,
									ALLEGRO_AUDIO_DEPTH_INT16,
									ALLEGRO_CHANNEL_CONF_2 , false );
#endif

							al_save_sample( _nstr( date_str ) , spl );
							al_destroy_sample( spl );

							free_nstr( &command );
							free_nstr( &date_str );
							}
							cur_time = 0 ;
							qvstudio -> record_buffer_pos = qvstudio -> record_buffer ;  /* points to the current recorded position */
							al_flush_event_queue( qvstudio -> queue );
						}
						else
						{
							qvstudio -> is_recording = 1 ;
						}
						low = high = 0 ;
					}
					break;
                default:
                    break;
			}
		}
		if( qvstudio -> is_recording == 0 && ( event.type == ALLEGRO_EVENT_DISPLAY_CLOSE ||
		(event.type == ALLEGRO_EVENT_KEY_UP && event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) ) )
		{
			if( qvstudio -> is_editing == 0 )
				goto clean_exit ;
		}
		else if( event.type == ALLEGRO_EVENT_TIMER )
		{
			if( al_get_timer_event_source( qvstudio -> fps_timer ) == event.any.source )
			{
				fps = ( fps + fps_count ) / 2 ;
				fps_count = 0 ;
			}
			else if( al_get_timer_event_source( qvstudio -> timer ) == event.any.source )
			{
				do_draw = true;
				if( qvstudio -> is_recording == 1 )
					cur_time += 1.0 / qvstudio -> refresh_rate ;
				else
					cur_time = 0 ;
			}
		}

		if( do_draw == true )
		{
			fps_count ++ ;
			al_set_target_bitmap( scrbuf );

			w = qvstudio -> w ;
			h = qvstudio -> h ;

			al_clear_to_color(al_map_rgb(0,0,0));
			if( qvstudio -> bg )
				al_draw_bitmap( qvstudio -> bg , 0, 0, 0);

			/*show title list */
			draw_basic_waveform( qvstudio );
			draw_title_list( qvstudio );
			draw_volume( qvstudio );

			ALLEGRO_COLOR color ;
			ALLEGRO_FONT *cur_font = NULL ;

			N_STR *rec_info = NULL ;
			long int hours = cur_time/(60 * 60);
			long int minutes = (int)( cur_time / 60 )%60;
			long int num_seconds = (int)(cur_time )%60 ;
			long int msec = 1000 * ( cur_time - ( (60*60)*hours + 60 * minutes + num_seconds ) );

			color = al_map_rgb(0,255,0);
			if( qvstudio -> is_recording == 1 )
			{
				nstrprintf( rec_info , "RECORDING: %02ld:%02ld:%03ld" , minutes , num_seconds , msec );
				cur_font = qvstudio -> font[ HUGE_FONT ];
				color = al_map_rgb(255,0,0);
				al_draw_text( cur_font, color , w/2 , h - 150  , ALLEGRO_ALIGN_CENTER , _nstr( rec_info ) );
				free_nstr( &rec_info );
			}
#ifdef FLOAT_AUDIO
			nstrprintf( rec_info , "gain:%1.3f low: %1.3f , high: %1.3f , nb_disp=%d , nb_titles: %d, fps: %d" , qvstudio -> gain  , low , high ,qvstudio -> nb_displayed_samples , qvstudio -> nb_titles , fps );
#else
			nstrprintf( rec_info , "gain:%1.3f low:%4.4d , high:%4.4d  nb_disp=%d , nb_titles: %d, fps: %d" , qvstudio -> gain  , low , high ,qvstudio -> nb_displayed_samples , qvstudio -> nb_titles , fps );
#endif

			cur_font = qvstudio -> font[ SMALL_FONT ];
			/* recording info */
			al_draw_text( cur_font, color , 10 , 10 , 0 , _nstr( rec_info ) );
			/* glinfos */
			al_draw_text( cur_font, color , 10 , h - 30 , 0 ,  (const char *)renderer );
			free_nstr( &rec_info );

			/* display current title */
			if( qvstudio -> is_editing == 0 )
			{
				if( qvstudio -> nb_titles > 0 )
					al_draw_ustr( qvstudio -> font[ HUGE_FONT ] , color , w/2 , h/2 , ALLEGRO_ALIGN_CENTER, qvstudio -> titles[ selected_title ] );
			}
			else
			{
				ftime(&tmb);
				if( (tmb.millitm%500) > 250 )
					disp_carret = 1 ;
				else
					disp_carret = 0 ;

				al_draw_text( qvstudio -> font[ HUGE_FONT ] , color , w/2 , h/2 - 50  , ALLEGRO_ALIGN_CENTER, "[TITLENAME EDIT]" );
				if( disp_carret == 1 )
					al_ustr_append_cstr( title , "_" );
				else
					al_ustr_append_cstr( title , " " );
				al_draw_ustr( qvstudio -> font[ HUGE_FONT ] , color , w/2 , h/2 , ALLEGRO_ALIGN_CENTER, title  );


				al_ustr_remove_chr( title , al_ustr_length( title ) - 1 );
			}

			al_acknowledge_resize( qvstudio -> display );
			w = al_get_display_width(  qvstudio -> display );
			h = al_get_display_height( qvstudio -> display );

			al_set_target_bitmap( al_get_backbuffer( qvstudio -> display ) );
			al_clear_to_color( al_map_rgba( 0 , 0 , 0 , 255 ) );
			al_draw_bitmap( scrbuf , w/2 - al_get_bitmap_width( scrbuf ) /2 , h/2 - al_get_bitmap_height( scrbuf ) / 2 , 0 );
			al_flip_display();
			do_draw = false ;
		}
	}

clean_exit:
	if (qvstudio -> recorder)
	{
		al_destroy_audio_recorder( qvstudio -> recorder );
	}

	al_free( qvstudio -> record_buffer);

	return 0;
}



