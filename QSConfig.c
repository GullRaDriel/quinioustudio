#include "QSConfig.h"



void abort_qvstudio(char const *format, ...)
{
   char str[1024];
   va_list args;
   ALLEGRO_DISPLAY *display;

   va_start(args, format);
   vsnprintf(str, sizeof str, format, args);
   va_end(args);

   if (al_init_native_dialog_addon()) {
      display = al_is_system_installed() ? al_get_current_display() : NULL;
      al_show_native_message_box(display, "Error", "Cannot run QuiniouStudio", str, NULL, 0);
   }
   else {
      fprintf(stderr, "%s", str);
   }
   exit(1);
}

 /* get a color from a r,g,b,a string */
ALLEGRO_COLOR get_color_from_string( const char *str , char *cfg_param )
{
   __n_assert( str , abort_qvstudio( "Couldn't read %s to make a color" , cfg_param ) );

   char **result=split( str , "," , 0 );
   int count = split_count( result );
   if( count != 4 )
   {
      free_split_result( &result );
      abort_qvstudio( "Invalid number of colors (must be r,g,b,a): %d" , count );
   }

   int r = 255 , g = 0 , b = 0 , a = 0 ;
   str_to_int( trim_nocopy( result[ 0 ] ), &r , 10 );
   str_to_int( trim_nocopy( result[ 1 ] ), &g , 10 );
   str_to_int( trim_nocopy( result[ 2 ] ), &b , 10 );
   str_to_int( trim_nocopy( result[ 3 ] ), &a , 10 );
   free_split_result( &result );

   return al_map_rgba( r , g , b , a );
}



QS_INTERNALS *load_qs_config( char *file )
{
   QS_INTERNALS *internals = NULL ;
   ALLEGRO_CONFIG *cfg = NULL ;
   const char *strptr = NULL ;

   int tmpval = 0 ;

   cfg = al_load_config_file( file );
   if( !cfg )
   {
      abort_qvstudio( "Couldn't load config file %s\n" , file );
   }

   Malloc( internals , QS_INTERNALS , 1 );
   if( !internals )
   {
      abort_qvstudio( "Couldn't Malloc QVS_INTERNALS\n" );
   }

   /* DISPLAY */
   if( !(internals -> font_file_small = al_get_config_value(cfg, "DISPLAY", "font_file_small") ) )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] font_file_small\n" );
   }
   fprintf( stderr , "font_file_small: %s\n" , internals -> font_file_small );
   if( !(internals -> font_file_regular = al_get_config_value(cfg, "DISPLAY", "font_file_regular") ) )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] font_file_regular\n" );
   }
   fprintf( stderr , "font_file_regular: %s\n" , internals -> font_file_regular );
   if( !(internals -> font_file_huge = al_get_config_value(cfg, "DISPLAY", "font_file_huge") ) )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] font_file_huge\n" );
   }
   fprintf( stderr , "font_file_huge: %s\n" , internals -> font_file_huge );

   if( !(internals -> background_file = al_get_config_value(cfg, "DISPLAY", "background_file") ) )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] background_file\n" );
   }
   fprintf( stderr , "background_file: %s\n" , internals -> background_file );

   internals -> font_size_small = 0 ;
   if( str_to_int( al_get_config_value(cfg, "DISPLAY", "font_size_small") , &internals -> font_size_small , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] font_size_small\n" );
   }
   fprintf( stderr , "font_size_small: %d\n" , internals -> font_size_small );
   internals -> font_size_regular = 0 ;
   if( str_to_int( al_get_config_value(cfg, "DISPLAY", "font_size_regular" ) , &internals -> font_size_regular , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] font_size_regular\n" );
   }
   fprintf( stderr , "font_size_regular: %d\n" , internals -> font_size_regular );
   internals -> font_size_huge = 0 ;
   if( str_to_int( al_get_config_value(cfg, "DISPLAY", "font_size_huge") , &internals -> font_size_huge , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] font_size_huge\n" );
   }
   fprintf( stderr , "font_size_huge: %d\n" , internals -> font_size_huge );
   internals -> w = 0 ;
   if( str_to_int( al_get_config_value(cfg, "DISPLAY", "w") , &internals -> w , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] w\n" );
   }
   fprintf( stderr , "w: %d\n" , internals -> w );
   internals -> h = 0 ;
   if( str_to_int( al_get_config_value(cfg, "DISPLAY", "h") , &internals -> h , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] h\n" );
   }
   fprintf( stderr , "h: %d\n" , internals -> h );

   internals -> refresh_rate = 0 ;
   if( !( strptr = al_get_config_value(cfg, "DISPLAY", "refresh_rate") ) )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] refresh_rate\n" );
   }
   internals -> refresh_rate = strtod( strptr , NULL );
   if(  internals -> refresh_rate < 1.0 || internals -> refresh_rate >= HUGE_VAL )
   {
      abort_qvstudio( "[RECORDER] latency: incorrect float number\n" );
   }
   fprintf( stderr , "refresh_rate: %f\n" , internals -> refresh_rate );

   internals -> nb_viz_seconds = 0 ;
   if( !( strptr = al_get_config_value(cfg, "DISPLAY", "nb_viz_seconds") ) )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] nb_viz_seconds\n" );
   }
   internals -> nb_viz_seconds = strtod( strptr , NULL );
   if(  internals -> nb_viz_seconds < 0.1 || internals -> nb_viz_seconds >= HUGE_VAL )
   {
      abort_qvstudio( "[RECORDER] nb_viz_seconds: incorrect float number\n" );
   }
   fprintf( stderr , "nb_viz_seconds: %f\n" , internals -> nb_viz_seconds );

   internals -> fullscreen = 0 ;
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "DISPLAY", "fullscreen") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [DISPLAY] fullscreen\n" );
   }
   internals -> fullscreen = tmpval ;
   fprintf( stderr , "fullscreen: %d\n" , internals -> fullscreen );

   /* RECORDER */
   internals -> recorder = NULL ;
   internals -> record_buffer = NULL ;
   internals -> record_buffer_pos = NULL ;
   internals -> record_buffer_end = NULL ;
   internals -> is_recording = false ;

   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "RECORDER", "nb_min_secs") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [RECORDER] nb_min_secs\n" );
   }
   internals -> nb_min_secs = tmpval ;

   fprintf( stderr , "frequency: %d\n" , internals -> frequency );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "RECORDER", "frequency") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [RECORDER] frequency\n" );
   }
   internals -> frequency = tmpval ;
   fprintf( stderr , "frequency: %d\n" , internals -> frequency );

   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "RECORDER", "max_seconds") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [RECORDER] max_seconds\n" );
   }
   internals -> max_seconds = tmpval ;
   fprintf( stderr , "max_seconds: %d\n" , internals -> max_seconds );

   if( !( strptr = al_get_config_value(cfg, "RECORDER", "fragment") ) )
   {
      abort_qvstudio( "Couldn't load config [RECORDER] fragment\n" );
   }
   internals -> fragment = strtod( strptr , NULL );
   if(  internals -> fragment < 1.0 || internals -> fragment >= HUGE_VAL )
   {
      abort_qvstudio( "[RECORDER] fragment: incorrect float number\n" );
   }
   fprintf( stderr , "latency: %f\n" , internals -> fragment );

   if( !( strptr = al_get_config_value(cfg, "RECORDER", "latency") ) )
   {
      abort_qvstudio( "Couldn't load config [RECORDER] latency\n" );
   }
   internals -> latency = strtod( strptr , NULL );
   if(  internals -> latency < 0.0 || internals -> latency >= HUGE_VAL )
   {
      abort_qvstudio( "[RECORDER] latency: incorrect float number\n" );
   }
   fprintf( stderr , "latency: %f\n" , internals -> latency );

   /* initialize for safety */
   for( int it = 0 ; it <= NB_MAX_TITLES ; it ++ )
      internals -> titles[ it ] = NULL ;

   for( int it = 0 ; it <= 3 ; it ++ )
      internals -> font[ it ] = NULL ;

   internals -> gain = 0.0 ;
   internals -> queue = NULL ;
   internals -> timer = NULL ;
   internals -> display = NULL ;
   internals -> nb_titles = 0 ;

   /* [TITLE_LIST] */
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "TITLE_LIST", "x") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [TITLE_LIST] x\n" );
   }
   internals -> title_x = tmpval ;
   fprintf( stderr , "title_x: %d\n" , internals -> title_x );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "TITLE_LIST", "y") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [TITLE_LIST] y\n" );
   }
   internals -> title_y = tmpval ;
   fprintf( stderr , "title_y: %d\n" , internals -> title_y );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "TITLE_LIST", "w") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [TITLE_LIST] w\n" );
   }
   internals -> title_w = tmpval ;
   fprintf( stderr , "title_w: %d\n" , internals -> title_w );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "TITLE_LIST", "h") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [TITLE_LIST] h\n" );
   }
   internals -> title_h = tmpval ;
   fprintf( stderr , "title_h: %d\n" , internals -> title_h );

    /* [WAVEFORM] */
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "WAVEFORM", "x") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [WAVEFORM] x\n" );
   }
   internals -> wave_x = tmpval ;
   fprintf( stderr , "wave_x: %d\n" , internals -> wave_x );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "WAVEFORM", "y") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [WAVEFORM] y\n" );
   }
   internals -> wave_y = tmpval ;
   fprintf( stderr , "wave_y: %d\n" , internals -> wave_y );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "WAVEFORM", "w") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [WAVEFORM] w\n" );
   }
   internals -> wave_w = tmpval ;
   fprintf( stderr , "wave_w: %d\n" , internals -> wave_w );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "WAVEFORM", "h") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [WAVEFORM] h\n" );
   }
   internals -> wave_h = tmpval ;
   fprintf( stderr , "wave_h: %d\n" , internals -> wave_h );

  /* [VOLUME] */
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "VOLUME", "x") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [VOLUME] x\n" );
   }
   internals -> vol_x = tmpval ;
   fprintf( stderr , "vol_x: %d\n" , internals -> vol_x );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "VOLUME", "y") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [VOLUME] y\n" );
   }
   internals -> vol_y = tmpval ;
   fprintf( stderr , "vol_y: %d\n" , internals -> vol_y );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "VOLUME", "w") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [VOLUME] w\n" );
   }
   internals -> vol_w = tmpval ;
   fprintf( stderr , "vol_w: %d\n" , internals -> vol_w );
   tmpval = 0 ;
   if( str_to_int( al_get_config_value(cfg, "VOLUME", "h") , &tmpval , 10 ) != TRUE )
   {
      abort_qvstudio( "Couldn't load config [VOLUME] h\n" );
   }
   internals -> vol_h = tmpval ;
   fprintf( stderr , "vol_h: %d\n" , internals -> vol_h );


   /*
    * Config loading OK ; proceed initialisations
    */

   /* Saved title list */
   FILE *in = fopen( "title_list.txt"  ,"r" );
   if( !in )
   {
      abort_qvstudio("Unable to open title_list.txt\n");
   }
   /* max 256 bytes long */
   char title_buf[ 256 + 1 ] = "" ;
   while( fgets( title_buf , 256 , in ) && internals -> nb_titles < NB_MAX_TITLES )
   {
      internals -> titles[ internals -> nb_titles ] = al_ustr_new( title_buf );
	  al_ustr_trim_ws( internals -> titles[ internals -> nb_titles ] );
      internals -> nb_titles ++ ;
   }
   internals -> titles[  internals -> nb_titles ] = NULL ;
   fclose( in );

   /* allegro 5 + addons loading */
   if (!al_init()) {
      abort_qvstudio("Could not init Allegro.\n");
   }
   if (!al_install_audio()) {
      abort_qvstudio("Unable to initialize audio addon\n");
   }
   if (!al_init_acodec_addon()) {
      abort_qvstudio("Unable to initialize acoded addon\n");
   }
   if (!al_init_image_addon()) {
      abort_qvstudio("Unable to initialize image addon\n");
   }
   if (!al_init_primitives_addon() ) {
      abort_qvstudio("Unable to initialize primitives addon\n");
   }
   if( !al_init_font_addon() ) {
      abort_qvstudio("Unable to initialize font addon\n");
   }
   if( !al_init_ttf_addon() ) {
      abort_qvstudio("Unable to initialize ttf_font addon\n");
   }
   if( !al_install_keyboard() ) {
      abort_qvstudio("Unable to initialize keyboard handler\n");
   }
   if( !al_install_mouse()) {
      abort_qvstudio("Unable to initialize mouse handler\n");
   }

   if( internals -> fullscreen != 0 )
      al_set_new_display_flags( ALLEGRO_OPENGL|ALLEGRO_FULLSCREEN_WINDOW );
   else
      al_set_new_display_flags( ALLEGRO_OPENGL|ALLEGRO_WINDOWED );

   internals -> display = al_create_display( internals -> w , internals -> h );
   if( !internals -> display )
   {
      abort_qvstudio("Unable to create display\n");
   }
   al_set_new_bitmap_flags( ALLEGRO_VIDEO_BITMAP );

   internals -> font[ 0 ] = al_load_font( internals -> font_file_small , internals -> font_size_small , 0 );
   if (! internals -> font[ 0 ] ) {
      abort_qvstudio("Unable to load %s\n" , internals -> font_file_small );
   }
   internals -> font[ 1 ] = al_load_font( internals -> font_file_regular , internals -> font_size_regular , 0 );
   if (! internals -> font[ 0 ] ) {
      abort_qvstudio("Unable to load %s\n" , internals -> font_file_regular );
   }
   internals -> font[ 2 ] = al_load_font( internals -> font_file_huge , internals -> font_size_huge , 0 );
   if (! internals -> font[ 0 ] ) {
      abort_qvstudio("Unable to load %s\n" , internals -> font_file_huge );
   }
   internals -> channels = 2 ;
   internals -> recorder = al_create_audio_recorder(
         internals -> fragment / internals -> latency ,
         internals -> frequency * internals -> latency ,  /* configure the fragment size to give us the given
                                                             latency in seconds */
         internals -> frequency,   /* samples per second (higher => better quality) */
#ifdef FLOAT_AUDIO
         ALLEGRO_AUDIO_DEPTH_FLOAT32,   /* 2-byte sample size */
#else
         ALLEGRO_AUDIO_DEPTH_INT16,   /* 2-byte sample size */
#endif
         ALLEGRO_CHANNEL_CONF_2    /* stereo */
         );

   if ( !internals -> recorder )
   {
      abort_qvstudio("Unable to create audio recorder\n");
   }
   internals -> record_event = NULL ;
   internals -> tmp_buffer_size = internals -> channels * internals -> frequency * internals -> nb_viz_seconds  ;
   internals -> tmp_buffer = al_calloc( (int)( internals -> tmp_buffer_size ) , sizeof(audio_t));



   /* store up to max_seconds  ,  channel * freq * seconds */
   internals -> record_buffer = al_calloc( internals -> channels * internals -> frequency * internals -> max_seconds, sizeof(audio_t));
   internals -> record_buffer_pos = internals -> record_buffer ;
   internals -> record_buffer_end = internals -> record_buffer + internals -> channels * internals -> frequency * internals -> max_seconds ;

   internals -> queue = al_create_event_queue();
   if( !internals -> queue )
   {
      abort_qvstudio("Unable to create event queue\n");
   }
   internals -> timer = al_create_timer( 1.0 / internals -> refresh_rate );
   internals -> fps_timer = al_create_timer( 1.0 );

   al_register_event_source( internals -> queue, al_get_display_event_source( internals -> display ) );
   al_register_event_source( internals -> queue, al_get_audio_recorder_event_source( internals -> recorder ) );
   al_register_event_source( internals -> queue, al_get_timer_event_source( internals -> timer ) );
   al_register_event_source( internals -> queue, al_get_timer_event_source( internals -> fps_timer ) );
   al_register_event_source( internals -> queue, al_get_keyboard_event_source() );
   al_register_event_source( internals -> queue, al_get_mouse_event_source()) ;

   al_start_timer( internals -> timer );
   al_start_timer( internals -> fps_timer );
   al_start_audio_recorder( internals -> recorder );

   internals -> bg = al_load_bitmap( internals -> background_file );

   internals -> title_colors[ 0 ] = get_color_from_string( al_get_config_value( cfg, "TITLE_LIST" , "title_color_standby" ) ,"title_color_standby" );
   internals -> title_colors[ 1 ] = get_color_from_string( al_get_config_value( cfg, "TITLE_LIST" , "title_color_recording" ), "title_color_recording" );
   internals -> title_colors[ 2 ] = get_color_from_string( al_get_config_value( cfg, "TITLE_LIST" , "border_color_standby" ) ,"border_color_standby" );
   internals -> title_colors[ 3 ] = get_color_from_string( al_get_config_value( cfg, "TITLE_LIST" , "border_color_recording" ) ,"border_color_recording" );

   internals -> wave_colors[ 0 ] = get_color_from_string( al_get_config_value( cfg, "WAVEFORM" , "min_color_standby" ) ,"min_color_standby" );
   internals -> wave_colors[ 1 ] = get_color_from_string( al_get_config_value( cfg, "WAVEFORM" , "max_color_standby" ), "max_color_standby" );
   internals -> wave_colors[ 2 ] = get_color_from_string( al_get_config_value( cfg, "WAVEFORM" , "min_color_recording" ) ,"min_color_recording" );
   internals -> wave_colors[ 3 ] = get_color_from_string( al_get_config_value( cfg, "WAVEFORM" , "max_color_recording" ) ,"max_color_recording" );

   internals -> vol_colors[ 0 ] = get_color_from_string( al_get_config_value( cfg, "VOLUME" , "min_color_standby" ) ,"min_color_standby" );
   internals -> vol_colors[ 1 ] = get_color_from_string( al_get_config_value( cfg, "VOLUME" , "max_color_standby" ), "max_color_standby" );
   internals -> vol_colors[ 2 ] = get_color_from_string( al_get_config_value( cfg, "VOLUME" , "min_color_recording" ) ,"min_color_recording" );
   internals -> vol_colors[ 3 ] = get_color_from_string( al_get_config_value( cfg, "VOLUME" , "max_color_recording" ) ,"max_color_recording" );

   internals -> title_scroll_position = 0 ;
   internals -> is_editing = 0 ;

   return internals ;
}
