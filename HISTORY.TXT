15-11-2016 -added:  -convert output to output_mp3 with script, using ffmpeg
14-11-2016 -added:  -title edition
					-title deletion
					-title modification
			-Fixed: -event queue
					-cpu hogging
14-03-2016 -Fixed: -colors and various platform specific stuff
                   -saving sample when current is full
                   -creating directories on Win32
                   -display problem between Linux_x64 and Win7_x32
13-03-2016 -Fixed problem with waveform
           -added FLOAT_AUDIO for float32 encoding
12-03-2016 -Fixed queue problem 
           -Added two func in place of hard coded gfxs
11-03-2016 Original bugfix release
