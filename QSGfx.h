#ifndef HEADER_EF2D0D8BF9E67744
#define HEADER_EF2D0D8BF9E67744

#ifndef QuiniouStudioGfxHeader
#define QuiniouStudioGfxHeader

#ifdef __cplusplus
extern "C"
{
#endif

#include "nilorea/n_common.h"
#include "QSConfig.h"
#include "float.h"

	int get_keyboard( ALLEGRO_USTR *str , ALLEGRO_EVENT event );
	int draw_title_list( QS_INTERNALS *qvstudio );
	int draw_volume(  QS_INTERNALS *qvstudio );
	int draw_basic_waveform( QS_INTERNALS *qvstudio );


#ifdef __cplusplus
}
#endif

#endif
#endif // header guard 

