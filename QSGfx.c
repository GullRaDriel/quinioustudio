#include "QSGfx.h"


int get_keyboard( ALLEGRO_USTR *str , ALLEGRO_EVENT event )
{
	__n_assert( str , return FALSE );
	if( event.type == ALLEGRO_EVENT_KEY_CHAR )
	{
		if(event.keyboard.unichar >= 32)
		{
			al_ustr_append_chr(str, event.keyboard.unichar);
		}
		else if(event.keyboard.keycode == ALLEGRO_KEY_BACKSPACE)
		{
			int pos = (int)al_ustr_size(str);
			if(al_ustr_prev(str, &pos))
				al_ustr_truncate(str, pos);
		}
	}
	else
	{
		return FALSE ;
	}
	return TRUE ;
}


int draw_title_list( QS_INTERNALS *qvstudio )
{
	__n_assert( qvstudio , return FALSE );

	static ALLEGRO_BITMAP *title_bmp = NULL ;
	ALLEGRO_BITMAP *oldtarget = al_get_target_bitmap() ;
	if( !title_bmp )
	{
		title_bmp = al_create_bitmap( qvstudio -> title_w, qvstudio -> title_h );
		if( !title_bmp )
		{
			abort_qvstudio( "Unable to create title bitmap of size %d,%d" , qvstudio -> title_w, qvstudio -> title_h );
		}
	}

	al_set_target_bitmap( title_bmp );

	al_clear_to_color( al_map_rgba( 0 , 0 , 0 , 155 ) );

	int font_height = al_get_font_line_height( qvstudio -> font [ REGULAR_FONT ] );

	/*show title list */
	int it = 0 ;
	int offset_y = qvstudio -> title_scroll_position * font_height ;
	while( qvstudio -> titles[ it ] )
	{
		al_draw_ustr( qvstudio -> font[ REGULAR_FONT ] , qvstudio -> title_colors[ qvstudio -> is_recording ] ,
				3 , font_height * it - offset_y ,
				ALLEGRO_ALIGN_LEFT , qvstudio -> titles[ it ]
				);
		al_draw_rectangle( 1 , 1 + font_height * it ,
				qvstudio -> title_w , font_height * ( it + 1 ) ,
				qvstudio -> title_colors[ qvstudio -> is_recording ] , 1.0
				);
		it ++ ;
	}
	al_draw_filled_rectangle( qvstudio -> title_w - 20 , 0 , qvstudio -> title_w , qvstudio -> title_h ,
			al_map_rgba( 0 , 0 , 0 , 255 )
			);
	al_draw_rectangle( qvstudio -> title_w - 20 , 0 , qvstudio -> title_w , qvstudio -> title_h - 1.0 ,
			qvstudio -> title_colors[ qvstudio -> is_recording ] , 1.0
			);
	if( qvstudio -> nb_titles > 0 )
		al_draw_filled_rectangle( qvstudio -> title_w - 20 , ( qvstudio -> title_scroll_position * qvstudio -> title_h ) / qvstudio -> nb_titles ,
			qvstudio -> title_w      , 20 + ( qvstudio -> title_scroll_position * qvstudio -> title_h ) / qvstudio -> nb_titles ,
			qvstudio -> title_colors[ qvstudio -> is_recording ]
			);
	al_draw_rectangle( 0 , 0 , qvstudio -> title_w - 1 , qvstudio -> title_h - 1 ,
			qvstudio -> title_colors[ qvstudio -> is_recording ] , 1.0
			);




	al_set_target_bitmap( oldtarget );

	al_draw_bitmap( title_bmp , qvstudio -> title_x , qvstudio -> title_y , 0 );

	return TRUE ;
}


static float clamp(float x)
{
	if (x > 1)
		return 1;
	if(x<0)
		return 0 ;

	return x;
}

int draw_volume( QS_INTERNALS *qvstudio )
{
	__n_assert( qvstudio , return FALSE );

	float r1,g1,b1,a1,r2,g2,b2,a2 ;

	al_unmap_rgba_f( qvstudio -> vol_colors[ 2 * qvstudio -> is_recording ] , &r1 , &g1 , &b1 , &a1 );
	al_unmap_rgba_f( qvstudio -> vol_colors[ 2 * qvstudio -> is_recording + 1 ] , &r2 , &g2 , &b2 , &a2 );

	float r,g,b,a ;
	r = clamp( r1 + ( r2 - r1 ) * qvstudio -> gain );
	g = clamp( g1 + ( g2 - g1 ) * qvstudio -> gain );
	b = clamp( b1 + ( b2 - b1 ) * qvstudio -> gain );
	a = clamp( a1 + ( a2 - a1 ) * qvstudio -> gain );

	ALLEGRO_COLOR color = al_map_rgba_f( r , g , b , a );
	/* draw volume meter */
	al_draw_filled_rectangle( qvstudio -> vol_x , qvstudio -> vol_y , qvstudio -> vol_x + qvstudio -> vol_w , qvstudio -> vol_y + qvstudio -> vol_h , al_map_rgb( 0 , 0 , 0 ) );
	al_draw_filled_rectangle( qvstudio -> vol_x , qvstudio -> vol_y ,
			qvstudio -> vol_x + qvstudio -> vol_w , qvstudio -> vol_y + qvstudio -> vol_h * qvstudio -> gain ,
			color );
	return TRUE ;
}


int draw_basic_waveform( QS_INTERNALS *qvstudio )
{
	static ALLEGRO_BITMAP *wave_bmp = NULL ;
	ALLEGRO_BITMAP *oldtarget = al_get_target_bitmap() ;

	__n_assert( qvstudio , return FALSE );
	__n_assert( qvstudio -> tmp_buffer , return FALSE );

	if( !wave_bmp )
	{
		wave_bmp  = al_create_bitmap( qvstudio -> wave_w, qvstudio -> wave_h );
		if( !wave_bmp )
		{
			abort_qvstudio( "Unable to create title bitmap of size %d,%d" , qvstudio -> title_w, qvstudio -> title_h );
		}
	}

	al_set_target_bitmap( wave_bmp );

	al_clear_to_color( al_map_rgba( 0 , 0 , 0 , 155 ) );

	float lowg = 0 , highg = 0 ;
	audio_t low = 0 , high = 0 ;
#ifdef FLOAT_AUDIO
	float value = 0 ;
#else
	long int value = 0 ;
#endif

	if( qvstudio -> nb_displayed_samples <  2000 )
		qvstudio -> nb_displayed_samples =  2000 ;
	if( (unsigned)qvstudio -> nb_displayed_samples >= qvstudio -> tmp_buffer_size )
		qvstudio -> nb_displayed_samples =  qvstudio -> tmp_buffer_size -1  ;

	unsigned int  j=0 ;
	int  inc  =  qvstudio -> nb_displayed_samples / qvstudio -> wave_w ;

	float r1,g1,b1,a1,r2,g2,b2,a2,rlow,glow,blow,alow,rhigh,ghigh,bhigh,ahigh;

	al_unmap_rgba_f( qvstudio -> wave_colors[ 2*qvstudio -> is_recording ] , &r1 , &g1 , &b1 , &a1 );
	al_unmap_rgba_f( qvstudio -> wave_colors[ 2*qvstudio -> is_recording+1] , &r2 , &g2 , &b2 , &a2 );

	unsigned int itx_inc = 2 ;

	for( unsigned int itx = 0 ; itx < (unsigned)qvstudio -> wave_w ; itx += itx_inc )
	{

		/* Take the average of R samples so it fits on the screen */
		/* left channel */
		value = low = high = 0 ;
		for( j = next_even( itx * inc ) ; j < ((itx+itx_inc) * inc ) && j < qvstudio -> tmp_buffer_size ; j += 2 )
		{
			value += qvstudio -> tmp_buffer[qvstudio-> tmp_buffer_size - j ] ;
			equal_if( low  , > , qvstudio -> tmp_buffer[ qvstudio -> tmp_buffer_size - j ] );
			equal_if( high , < , qvstudio -> tmp_buffer[ qvstudio -> tmp_buffer_size - j ] );
		}
		value /= inc ;
		value /= 2 ;

#ifndef FLOAT_AUDIO
		lowg  = 2 * ( ( (low   - min_sample_val) / ((float) sample_range) ) - 0.5 );
		highg = 2 * ( ( (high  - min_sample_val) / ((float) sample_range) ) - 0.5 );
#endif


		rlow = clamp( r1 + ( r2 - r1 ) * fabs(lowg) );
		glow = clamp( g1 + ( g2 - g1 ) * fabs(lowg) );
		blow = clamp( b1 + ( b2 - b1 ) * fabs(lowg) );
		alow = clamp( a1 + ( a2 - a1 ) * fabs(lowg) );
		rhigh = clamp( r1 + ( r2 - r1 ) * highg );
		ghigh = clamp( g1 + ( g2 - g1 ) * highg );
		bhigh = clamp( b1 + ( b2 - b1 ) * highg );
		ahigh = clamp( a1 + ( a2 - a1 ) * highg );
		ALLEGRO_COLOR color_low  = al_map_rgba_f( rlow , glow , blow , alow );
		ALLEGRO_COLOR color_high = al_map_rgba_f( rhigh , ghigh , bhigh , ahigh );

		al_draw_line(  itx     , qvstudio -> wave_h/4        , itx , qvstudio -> wave_h/4 + highg * qvstudio -> wave_h/4 , color_high , itx_inc );
		al_draw_line(  itx     , qvstudio -> wave_h/4        , itx , qvstudio -> wave_h/4 + lowg  * qvstudio -> wave_h/4 , color_low  , itx_inc );

		/* draw right */
		value = low = high = 0 ;
		for( j = next_odd( itx * inc ) ; j < ( (itx+itx_inc)* inc ) && j < qvstudio -> tmp_buffer_size ; j += 2 )
		{
			value += qvstudio -> tmp_buffer[ qvstudio-> tmp_buffer_size - j ] ;
			equal_if( low  , > , qvstudio -> tmp_buffer[ qvstudio -> tmp_buffer_size - j ] );
			equal_if( high , < , qvstudio -> tmp_buffer[ qvstudio -> tmp_buffer_size - j ] );
		}
		value /= inc ;
		value /= 2 ;
#ifndef FLOAT_AUDIO
		lowg  = 2 * ( ( (low   - min_sample_val) / ((float) sample_range) ) - 0.5 );
		highg = 2 * ( ( (high  - min_sample_val) / ((float) sample_range) ) - 0.5 );
#endif

		rlow = clamp( r1 + ( r2 - r1 ) * fabs(lowg) );
		glow = clamp( g1 + ( g2 - g1 ) * fabs(lowg) );
		blow = clamp( b1 + ( b2 - b1 ) * fabs(lowg) );
		alow = clamp( a1 + ( a2 - a1 ) * fabs(lowg) );
		rhigh = clamp( r1 + ( r2 - r1 ) * highg );
		ghigh = clamp( g1 + ( g2 - g1 ) * highg );
		bhigh = clamp( b1 + ( b2 - b1 ) * highg );
		ahigh = clamp( a1 + ( a2 - a1 ) * highg );
		color_low  = al_map_rgba_f( rlow , glow , blow , alow );
		color_high = al_map_rgba_f( rhigh , ghigh , bhigh , ahigh );

		al_draw_line(  itx     , qvstudio -> wave_h - qvstudio -> wave_h/4 , itx , qvstudio -> wave_h - qvstudio -> wave_h/4 + highg * qvstudio -> wave_h/4 , color_high , itx_inc );
		al_draw_line(  itx     , qvstudio -> wave_h - qvstudio -> wave_h/4 , itx , qvstudio -> wave_h - qvstudio -> wave_h/4 + lowg  * qvstudio -> wave_h/4 , color_low  , itx_inc );
	}
	al_draw_rectangle( 0 , 0 , qvstudio ->wave_w , qvstudio ->wave_h ,  al_map_rgb(0,255,0) , 1.5 );

	al_set_target_bitmap( oldtarget );

	al_draw_bitmap( wave_bmp , qvstudio -> wave_x , qvstudio -> wave_y , 0 );

	return TRUE ;
}
