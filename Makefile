RM=rm -f
CC=gcc
OPT=-std=c11 -Og -g -ggdb3 -Wall -Wextra -Wcast-align -Wcast-qual -Wdisabled-optimization -Wformat=1 -Winit-self -Wlogical-op -Wmissing-include-dirs -Wredundant-decls -fno-strict-overflow -Wswitch-default -Wundef -Wno-unused
VPATH=../../LIB/src/
CC=gcc
LIBNILOREA=-lnilorea
EXT=
ALLEGRO_CLIBS=
HAVE_ALLEGRO=1
DEBUGLIB=
DEBUG_MEM=0

ifeq ($(OS),Windows_NT)
	CFLAGS+= -I../../LIB/include -D__USE_MINGW_ANSI_STDIO $(OPT)
    CLIBS=-Wl,-Bstatic -lpthread -lws2_32 -lz -Wl,-Bdynamic -lpcre
    RM= del /Q
	CC= gcc
	DEBUGLIB=-limagehlp
	ifeq ($(MSYSTEM),MINGW32)
		RM=rm -f
		CFLAGS+= -m32 -DARCH32BITS
		LIBNILOREA=-lnilorea32
		EXT=32.exe
		endif
		ifeq ($(MSYSTEM),MINGW32CB)
		RM=del /Q
		CFLAGS+= -m32 -DARCH32BITS
		LIBNILOREA=-lnilorea32
		EXT=32.exe
	endif

	ifeq ($(MSYSTEM),MINGW64)
		RM=rm -f
		CFLAGS+= -DARCH64BITS
		LIBNILOREA=-lnilorea64
		EXT=64.exe
		endif
		ifeq ($(MSYSTEM),MINGW64CB)
		RM=del /Q
		CFLAGS+= -DARCH64BITS
		LIBNILOREA=-lnilorea64
		EXT=64.exe
	endif
else
	UNAME_S= $(shell uname -s)
	RM=rm -f
	CC=gcc
	EXT=
    ifeq ($(UNAME_S),Linux)
        VPATH=../../LIB/src/
        CFLAGS+= -I../../LIB/include -W -Wall -D_XOPEN_SOURCE=600 -D_XOPEN_SOURCE_EXTENTED -std=gnu99 $(OPT)
        CLIBS=-lpthread -lpcre -ldl -lm -lz
    endif
    ifeq ($(UNAME_S),SunOS)
        CLIBS=-lsocket -lnsl -lm -lpcre -lz
        CFLAGS+= -mt -g -v -xc99 -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -I../../LIB/include -DVERSION=\"4.0\" -I/usr/include/pcre/ -L/usr/lib/
        CC=cc
        LIBNILOREA=-lnilorea
        EXT=
    endif
endif

CFLAGS+= -DHAVE_STRUCT_TIMESPEC -D_GNU_SOURCE -DALLEGRO_UNSTABLE -mwindows
LDLIBS=-lallegro -lallegro_acodec -lpthread -lallegro_main -lallegro_primitives -lallegro_ttf -lallegro_audio -lallegro_font -lallegro_dialog -lallegro_image -lopengl32

APIOBJ= n_common.o n_list.o n_log.o n_str.o n_hash.o n_debug_mem.o n_signals.o n_zlib.o n_time.o n_network.o n_network_msg.o n_thread_pool.o n_pcre.o n_config_file.o
TARGET=QuiniouStudio$(EXT)
SRC=QuiniouStudio.c QSConfig.c QSGfx.c
OBJ=$(SRC:%.c=%.o)

%.o : %.c
	$(CC) $(CFLAGS) -o $@ -c $<

all: $(TARGET)

QuiniouStudio$(EXT): $(APIOBJ) $(OBJ)
	$(CC) $(CFLAGS) $(APIOBJ) $(OBJ) -o $@ $(CLIBS) $(LDLIBS) $(DEBUGLIB)

clean:
	$(RM) *.o
	$(RM) QuiniouStudio$(EXT)
