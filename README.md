**ENGLISH**

**Quiniou Studio**

Simple sample recorder with quick name tagging

Customise via the config file 

Use Nilorea, Allegro5 Available on Linux, Windows (Android port possible)


*Mouse:*

-select current title with a left click on the list

-edit title with a right click on it

-scroll to move in the list



*Keyboard:*

-KEY_F12: windowed / full_screen

-KEY_NUM_ENTER: add an empty title entry

-KEY_NUM_PLUS: increase the length of viewed time on the spectrum

-KEY_NUM_MINUS: decrease the lenght of viwed time on the spectrum

-KEY_ENTER: when editing a title, finish and save 

-KEY_SPACE: start /stop recording


*Informations:*

-You can not quit if a recording is still running

-You can not quit if a title edit is not finished

-To delete a title, save it as blank

-To cancel a title add, just save it as blank

-More params in QuiniouStudio.cfg




**FRENCH:**


**Quiniou Studio**

Enregistreur audio simple avec gestion de liste de titre et hordodatage.

Configuration via le fichier cfg

Use Nilorea, Allegro5

Available on Linux, Windows (Android port possible)


*Souris:*

-sélection du titre en cours avec click gauche 

-édition du texte du titre avec click droit

-déplacement de la liste avec le scroll



*Clavier:*

-KEY_F12: fenêtré / plein écran

-KEY_NUM_ENTER: ajoute un nouveau titre vide

-KEY_NUM_PLUS: augmente le nombre de secondes visualisées

-KEY_NUM_MINUS: réduit le nombre de secondes visualisées

-KEY_ENTER: en édition de titre termine l'édition

-KEY_SPACE: démarre ou arrête l'enregistrement



*Informations:*

-Il est impossible de quitter si il y a un enregistrement en cours

-Il est impossible de quitter si il y a une édition en cours

-Pour supprimer un titre il suffit de le mettre à blanc

-Pour annuler un ajout il suffit d'appuyer sur entrée et d'être à blanc

-D'autres paramètres sont accessibles dans QuiniouStudio.cfg


