#!/bin/bash
LAMEOPTS="-S"

for FILE in *.wav ; do
    OUTNAME=`basename "$FILE" .wav`.mp3
    lame $LAMEOPTS "$FILE" "$OUTNAME"
done

for FILE in *.mp3 ; do
    mp3splt -r "$FILE"
done
exit

